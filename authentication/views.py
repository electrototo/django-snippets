from django.contrib.auth import views as auth_views


class CustomLoginView(auth_views.LoginView):
    template_name = 'authentication/login.html'
